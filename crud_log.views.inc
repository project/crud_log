<?php
function crud_log_views_data() {
    // Basic table information.
    $data['crud_log']['table']['group']  = t('CRUD Log');

    // Advertise this table as a possible base table
    $data['crud_log']['table']['base'] = array(
        'field' => 'pid',
        'title' => t('CRUD Log'),
        'help' => t('CRUD Log Custom database table.'),
        'weight' => 10,
    );

    // Tweet ID
    $data['crud_log']['pid'] = array(
        'title' => t('Primary Key Id'),
        'help' => t('The ID of the CRUD Log record.'),
        'field' => array(
            'handler' => 'views_handler_field',
            'click sortable' => TRUE,
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_numeric',
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
        'argument' => array(
            'handler' => 'views_handler_argument_string',
        ),
    );

    //Description of User ID
    $data['crud_log']['uid'] = array(
        'title' => t('User Id'),
        'help' => t('The ID of user who performed the node operation.'),
        'field' => array(
            'handler' => 'views_handler_field',
            'click sortable' => TRUE,
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_numeric',
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
        'argument' => array(
            'handler' => 'views_handler_filter_numeric',
            'name field' => 'uid', // the field to display in the summary.
            'numeric' => TRUE,
            'validate type' => 'uid',
        ),
        'relationship' => array(
            'base' => 'users',
            'field' => 'uid',
            'handler' => 'views_handler_relationship',
            'label' => t('Users'),
        ),
    );

    //Description of timestamp field
    $data['crud_log']['date'] = array(
        'title' => t('Timestamp'),
        'help' => t('Timestamp of the operation.'),
        'field' => array(
            'handler' => 'views_handler_field_date',
            'click sortable' => TRUE,
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_date',
        ),
    );

    //Description of operations field
    $data['crud_log']['operation'] = array(
        'title' => t('Operation'),
        'help' => t('The operation: Create, Read, Update, Delete (c,r,u,d).'),
        'field' => array(
            'handler' => 'views_handler_field_xss',
            'click sortable' => TRUE,
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_string',
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
    );

    //Description of title field
    $data['crud_log']['title'] = array(
        'title' => t('Title'),
        'help' => t('The title of the entity.'),
        'field' => array(
            'handler' => 'views_handler_field_xss',
            'click sortable' => TRUE,
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_string',
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
    );

    //Description of type field
    $data['crud_log']['type'] = array(
        'title' => t('Object type'),
        'help' => t('The type of object, defaults to entity.'),
        'field' => array(
            'handler' => 'views_handler_field_xss',
            'click sortable' => TRUE,
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_string',
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
    );

    //Description of bundle field
    $data['crud_log']['bundle'] = array(
        'title' => t('Bundle'),
        'help' => t('The content type.'),
        'field' => array(
            'handler' => 'views_handler_field_xss',
            'click sortable' => TRUE,
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_string',
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
    );

    //Description of record ID
    $data['crud_log']['rid'] = array(
        'title' => t('Record ID'),
        'help' => t('The Node ID.'),
        'field' => array(
            'handler' => 'views_handler_field',
            'click sortable' => TRUE,
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_numeric',
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
        'argument' => array(
            'handler' => 'views_handler_argument_string',
        ),
        'relationship' => array(
            'base' => 'node',
            'field' => 'rid',
            'handler' => 'views_handler_relationship',
            'label' => t('Nodes'),
        ),
    );

    //Description of path alias field
    $data['crud_log']['path'] = array(
        'title' => t('Path Alias'),
        'help' => t('The clean url, if any.'),
        'field' => array(
            'handler' => 'views_handler_field_xss',
            'click sortable' => TRUE,
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_string',
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
    );

    //Description of machine path field
    $data['crud_log']['machine_path'] = array(
        'title' => t('Machine Path'),
        'help' => t('Drupal\'s internal url.'),
        'field' => array(
            'handler' => 'views_handler_field_xss',
            'click sortable' => TRUE,
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_string',
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
    );

    return $data;
}