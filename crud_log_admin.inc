<?php
/**
 * @return array|mixed
 * Called from hook_menu()
 */
function crud_log_admin() {
    return drupal_get_form('crud_log_admin_form');
}

/**
 * Implements hook_form().
 *
 * Admin page form
 */
function crud_log_admin_form($form, &$form_state) {
    $settings = variable_get('crud_log_settings');
    $form['description'] = array(
        '#type' => 'markup',
        '#markup' => t('This module captures node data into a custom table on the events selected below. The data is not directly accessible. It is exposed to Views as "CRUD Log".'),
    );
    $form['ops'] = array(
        '#title' => t('Node operations that will be logged.'),
        '#type' => 'checkboxes',
        '#options' => array('c' => t('Create'), 'r' => t('Read'), 'u' => t('Update'), 'd' => t('Delete')),
        '#default_value' => $settings['ops'],
        '#description' => t("It's recommended you leave 'Read' disabled. Logging node Read events will generate an large number of records and could impact site performance."),
    );
    $form['taxonomy_ops'] = array(
        '#title' => t('Taxonomy operations that will be logged.'),
        '#type' => 'checkboxes',
        '#options' => array('c' => t('Create'), 'r' => t('Read'), 'u' => t('Update'), 'd' => t('Delete')),
        '#default_value' => $settings['taxonomy_ops'],
        '#description' => t("It's recommended you leave 'Read' disabled. Logging taxonomy Read events will generate an large number of records and could impact site performance."),
    );
    $form['purge'] = array(
        '#title' => t('Delete old log records.'),
        '#type' => 'select',
        '#options' => array(
            0 => t('Do not delete'),
            86400 => t('1 day'),
            604800 => t('1 week'),
            1209600 => t('2 weeks'),
            2592000 => t('1 month'),
            7776000 => t('3 months'),
            15552000 => t('6 months'),
            31536000 => t('1 year'),
        ),
        '#default_value' => $settings['purge'],
        '#description' => t("Select the age of the records you want automatically deleted."),
    );

    $content_types = node_type_get_types();
    foreach($content_types as $type) {
        $types[$type->type] = $type->name;
    }
    $form['content_types'] = array(
        '#title' => t('Content types that will be logged.'),
        '#type' => 'checkboxes',
        '#options' => $types,
        '#default_value' => $settings['content_types'] ? $settings['content_types']: array(),
        '#description' => t("It's recommended you leave 'Read' disabled. Logging node Read events will balloon the database."),
    );
    $vocabularies = taxonomy_get_vocabularies();
    foreach($vocabularies as $vocab) {
        $vocabs[$vocab->vid] = $vocab->name;
    }
    $form['vocabularies'] = array(
        '#title' => t('Vocabularies that will be logged.'),
        '#type' => 'checkboxes',
        '#options' => $vocabs,
        '#default_value' => $settings['vocabularies'] ? $settings['vocabularies']: array(),
        '#description' => t("It's recommended you leave 'Read' disabled. Logging node Read events will balloon the database."),
    );

    $form['actions']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Submit'),
    );

    return $form;
}

/**
 * Implements hook_form_submit().
 *
 * Admin page form
 */
function crud_log_admin_form_submit($form, $form_state) {
    foreach($form_state['values']['ops'] as $op) {
        if ($op) {
            $settings['ops'][] = $op;
        }
    }
    foreach($form_state['values']['taxonomy_ops'] as $op) {
        if ($op) {
            $settings['taxonomy_ops'][] = $op;
        }
    }
    foreach($form_state['values']['content_types'] as $type) {
        if ($type) {
            $settings['content_types'][] = $type;
        }
    }
    foreach($form_state['values']['vocabularies'] as $vocab) {
        if ($vocab) {
            $settings['vocabularies'][] = $vocab;
        }
    }
    $settings['purge'] = $form_state['values']['purge'];

    variable_set('crud_log_settings', $settings);
    drupal_set_message(t('Your settings have been saved.'), 'status');

    if (in_array('r', $settings)) {
        drupal_set_message(t('You have set Node CRUD log to capture node read events. Be warned, this will generate a lot of log entries and could impact performance.'), 'warning');
    }
}