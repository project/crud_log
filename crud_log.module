<?php
/**
 * Implements hook_menu().
 */
function crud_log_menu() {
    $items['admin/config/system/crud-log'] = array(
        'title' => 'Crud Log Settings',
        'page callback' => 'crud_log_admin',
        'access arguments' => array('administer crud log'),
        'file' => 'crud_log_admin.inc',
    );
    return $items;
}

/**
 * Implements hook_permission().
 */
function crud_log_permission() {
    return array(
        'administer crud log' => array(
            'title' => t('Administer CRUD Log'),
        ),
    );
}

/**
 * Implements hook_cron().
 */
function crud_log_cron() {
    $settings = variable_get('crud_log_settings', array());
    if ($settings['purge']) {
        $threshold = time() - $settings['purge'];
        $logs_deleted = db_delete('crud_log')
            ->condition('date', $threshold, '<')
            ->execute();

        if ($logs_deleted) {
            watchdog('crud_log', '%num old CRUD Log records were deleted.', array('%num' => $logs_deleted), WATCHDOG_ALERT);
        }
    }
}

// REACT TO NODE EVENTS
/**
 * Implements hook_node_insert().
 */
function crud_log_node_insert($node) {
    $settings = variable_get('crud_log_settings', array());
    if (in_array('c', $settings['ops'])) {
        if (in_array($node->type, $settings['content_types'])) {
            _crud_log_capture_data($node, 'c');
        }
    }
}

/**
 * Implements hook_node_view().
 */
function crud_log_node_view($node, $view_mode, $langcode) {
    $settings = variable_get('crud_log_settings', array());
    if (in_array('r', $settings['ops'])) {
        if (in_array($node->type, $settings['content_types'])) {
            _crud_log_capture_data($node, 'r');
        }
    }
}

/**
 * Implements hook_node_update().
 */
function crud_log_node_update($node) {
    $settings = variable_get('crud_log_settings', array());
    if (in_array('u', $settings['ops'])) {
        if (in_array($node->type, $settings['content_types'])) {
            _crud_log_capture_data($node, 'u');
        }
    }
}

/**
 * Implements hook_node_delete().
 */
function crud_log_node_delete($node) {
    $settings = variable_get('crud_log_settings', array());
    if (in_array('d', $settings['ops'])) {
        if (in_array($node->type, $settings['content_types'])) {
            _crud_log_capture_data($node, 'd');
        }
    }
}

// React to Taxonomy Events
/**
 * Implements hook_taxonomy_term_view().
 */
function crud_log_taxonomy_term_view($term, $view_mode, $langcode) {
    $settings = variable_get('crud_log_settings', array());
    if (in_array('r', $settings['taxonomy_ops'])) {
        if (in_array($term->vid, $settings['vocabularies'])) {
            _crud_log_capture_data($term, 'r', 'term');
        }
    }
}

/**
 * Implements hook_taxonomy_term_insert().
 */
function crud_log_taxonomy_term_insert($term) {
    $settings = variable_get('crud_log_settings', array());
    if (in_array('c', $settings['taxonomy_ops'])) {
        if (in_array($term->vid, $settings['vocabularies'])) {
            _crud_log_capture_data($term, 'c', 'term');
        }
    }
}

/**
 * Implements hook_taxonomy_term_update().
 */
function crud_log_taxonomy_term_update($term) {
    $settings = variable_get('crud_log_settings', array());
    if (in_array('u', $settings['taxonomy_ops'])) {
        if (in_array($term->vid, $settings['vocabularies'])) {
            _crud_log_capture_data($term, 'u', 'term');
        }
    }
}

/**
 * Implements hook_taxonomy_term_delete().
 */
function crud_log_taxonomy_term_delete($term) {
    $settings = variable_get('crud_log_settings', array());
    if (in_array('d', $settings['taxonomy_ops'])) {
        if (in_array($term->vid, $settings['vocabularies'])) {
            _crud_log_capture_data($term, 'd', 'term');
        }
    }
}

/**
 * @param $entity
 * @param $op: allowed values 'c', 'r', 'u', 'd'
 * @param string $entity_type: allowed values 'node', 'term'
 * Custom function for inserting captured data into custom table
 */
function _crud_log_capture_data($entity, $op, $entity_type = 'node') {
    global $user;
    if ($entity_type == 'node') {
        $rid = $entity->nid;
        $title = $entity->title;
        $type = 'node';
        $bundle = $entity->type;
        $uid = $user->uid;
        $path = drupal_get_path_alias('node/' . $entity->nid, $entity->language);
        $machine_path = 'node/' . $entity->nid;
        if ($path == $machine_path) {
            $path = '';
        }
    }
    else if ($entity_type == 'term') {
        // make certain we capture the right value for bundle by loading the vocab here.
        $vocab = taxonomy_vocabulary_load($entity->vid);
        $rid = $entity->tid;
        $title = $entity->name;
        $type = 'term';
        $bundle = 'Vocabulary: ' . $vocab->name;
        $uid = $user->uid;
        if (isset($entity->language)) {
            $path = drupal_get_path_alias('taxonomy/term/' . $entity->tid, $entity->language);
        }
        else {
            $path = drupal_get_path_alias('taxonomy/term/' . $entity->tid);
        }
        $machine_path = 'taxonomy/term/' . $entity->tid;
        if ($path == $machine_path) {
            $path = '';
        }
    }

    db_insert('crud_log')
        ->fields(array(
            'operation' => $op,
            'rid' => $rid,
            'type' => $type,
            'bundle' => $bundle,
            'title' => $title,
            'path' => $path,
            'machine_path' => $machine_path,
            'date' => time(),
            'uid' => $uid,
        ))
        ->execute();
}

/**
 * Implements hook_views_api().
 * Expose the data to Views
 */
function crud_log_views_api() {
    return array(
        'api' => '3.0'
    );
}