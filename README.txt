This module was inspired by the Event Log module. The one problem I had with that module is that it captures all CRUD operations without the ability to ignore certain operations, specifically Read operations. Logging every Read operation would really fill up the database quickly and poses a performance problem.

This module will capture read events, but it recommends against it and the default setting is to ignore them.

It logs the events in a custom table and exposes the data to Views. There is a default View to get you started. You'll probably want to just clone and then disable it and make modifications to the clone.

The module was architectured to possibly support other entity types in the future, but for now, it's strictly nodes and taxonomy terms.