<?php
/**
 * @file
 * Default view for CRUD Log module.
 */
function crud_log_views_default_views() {
    $view = new view();
    $view->name = 'crud_log_report';
    $view->description = '';
    $view->tag = 'default';
    $view->base_table = 'crud_log';
    $view->human_name = t('CRUD Log Report');
    $view->core = 7;
    $view->api_version = '3.0';
    $view->disabled = FALSE;

    /* Display: Master */
    $handler = $view->new_display('default', 'Master', 'default');
    $handler->display->display_options['title'] = t('CRUD Log Report');
    $handler->display->display_options['use_ajax'] = TRUE;
    $handler->display->display_options['use_more_always'] = FALSE;
    $handler->display->display_options['access']['type'] = 'none';
    $handler->display->display_options['cache']['type'] = 'none';
    $handler->display->display_options['query']['type'] = 'views_query';
    $handler->display->display_options['exposed_form']['type'] = 'basic';
    $handler->display->display_options['pager']['type'] = 'full';
    $handler->display->display_options['pager']['options']['items_per_page'] = '50';
    $handler->display->display_options['style_plugin'] = 'table';
    $handler->display->display_options['style_options']['columns'] = array(
        'date' => 'date',
        'operation' => 'operation',
        'title' => 'title',
        'path' => 'path',
        'machine_path' => 'machine_path',
        'bundle' => 'bundle',
        'uid' => 'uid',
    );
    $handler->display->display_options['style_options']['default'] = '-1';
    $handler->display->display_options['style_options']['info'] = array(
        'date' => array(
            'sortable' => 1,
            'default_sort_order' => 'asc',
            'align' => '',
            'separator' => '',
            'empty_column' => 0,
        ),
        'operation' => array(
            'sortable' => 1,
            'default_sort_order' => 'asc',
            'align' => '',
            'separator' => '',
            'empty_column' => 0,
        ),
        'title' => array(
            'sortable' => 1,
            'default_sort_order' => 'asc',
            'align' => '',
            'separator' => '',
            'empty_column' => 0,
        ),
        'path' => array(
            'sortable' => 1,
            'default_sort_order' => 'asc',
            'align' => '',
            'separator' => '',
            'empty_column' => 0,
        ),
        'machine_path' => array(
            'sortable' => 1,
            'default_sort_order' => 'asc',
            'align' => '',
            'separator' => '',
            'empty_column' => 0,
        ),
        'bundle' => array(
            'sortable' => 1,
            'default_sort_order' => 'asc',
            'align' => '',
            'separator' => '',
            'empty_column' => 0,
        ),
        'uid' => array(
            'sortable' => 0,
            'default_sort_order' => 'asc',
            'align' => '',
            'separator' => '',
            'empty_column' => 0,
        ),
    );
    /* Field: CRUD Log: Timestamp */
    $handler->display->display_options['fields']['date']['id'] = 'date';
    $handler->display->display_options['fields']['date']['table'] = 'crud_log';
    $handler->display->display_options['fields']['date']['field'] = 'date';
    $handler->display->display_options['fields']['date']['label'] = t('Date');
    $handler->display->display_options['fields']['date']['date_format'] = 'short';
    $handler->display->display_options['fields']['date']['second_date_format'] = 'long';
    /* Field: CRUD Log: Operation */
    $handler->display->display_options['fields']['operation']['id'] = 'operation';
    $handler->display->display_options['fields']['operation']['table'] = 'crud_log';
    $handler->display->display_options['fields']['operation']['field'] = 'operation';
    /* Field: CRUD Log: Title */
    $handler->display->display_options['fields']['title']['id'] = 'title';
    $handler->display->display_options['fields']['title']['table'] = 'crud_log';
    $handler->display->display_options['fields']['title']['field'] = 'title';
    /* Field: CRUD Log: Path Alias */
    $handler->display->display_options['fields']['path']['id'] = 'path';
    $handler->display->display_options['fields']['path']['table'] = 'crud_log';
    $handler->display->display_options['fields']['path']['field'] = 'path';
    /* Field: CRUD Log: Machine Path */
    $handler->display->display_options['fields']['machine_path']['id'] = 'machine_path';
    $handler->display->display_options['fields']['machine_path']['table'] = 'crud_log';
    $handler->display->display_options['fields']['machine_path']['field'] = 'machine_path';
    /* Field: CRUD Log: Bundle */
    $handler->display->display_options['fields']['bundle']['id'] = 'bundle';
    $handler->display->display_options['fields']['bundle']['table'] = 'crud_log';
    $handler->display->display_options['fields']['bundle']['field'] = 'bundle';
    $handler->display->display_options['fields']['bundle']['label'] = t('Content Type');
    /* Field: CRUD Log: User Id */
    $handler->display->display_options['fields']['uid']['id'] = 'uid';
    $handler->display->display_options['fields']['uid']['table'] = 'crud_log';
    $handler->display->display_options['fields']['uid']['field'] = 'uid';
    $handler->display->display_options['fields']['uid']['label'] = t('User ID');
    $handler->display->display_options['fields']['uid']['element_label_colon'] = FALSE;
    /* Sort criterion: CRUD Log: Timestamp */
    $handler->display->display_options['sorts']['date']['id'] = 'date';
    $handler->display->display_options['sorts']['date']['table'] = 'crud_log';
    $handler->display->display_options['sorts']['date']['field'] = 'date';
    /* Filter criterion: CRUD Log: Timestamp */
    $handler->display->display_options['filters']['date']['id'] = 'date';
    $handler->display->display_options['filters']['date']['table'] = 'crud_log';
    $handler->display->display_options['filters']['date']['field'] = 'date';
    $handler->display->display_options['filters']['date']['operator'] = 'between';
    $handler->display->display_options['filters']['date']['exposed'] = TRUE;
    $handler->display->display_options['filters']['date']['expose']['operator_id'] = 'date_op';
    $handler->display->display_options['filters']['date']['expose']['label'] = t('Between');
    $handler->display->display_options['filters']['date']['expose']['description'] = t('Use the format CCYY-MM-DD HH:MM');
    $handler->display->display_options['filters']['date']['expose']['operator'] = 'date_op';
    $handler->display->display_options['filters']['date']['expose']['identifier'] = 'date';
    $handler->display->display_options['filters']['date']['expose']['remember_roles'] = array(
        2 => '2',
        1 => 0,
        3 => 0,
        4 => 0,
        5 => 0,
        6 => 0,
        7 => 0,
    );

  return array('crud_log' => $view);
}